const slides = document.querySelectorAll( ".slide" );
const timerTwoTimer = document.querySelector('.timer-timer');
const timerCounter = document.querySelector('.timer-counter');
const btnStop = document.querySelector('#end');
const btnResume = document.querySelector('#start');

let index = 0;
let timers = null;
let countS = 0;
let countMs = 1;


        slideTo = function( slide ) {
			slides.forEach(img => img.style.opacity = '0');
			slides[slide].style.opacity = '1';
		};

		action = function() {
			index++;
			if (index === slides.length) {
				index = 0;
			}
			if(countMs === 100){
				countS++;
				countMs = 1;
			}
			timerTwoTimer.innerText = `Счетчик sec: ${countS} msec : ${countMs}`;
			countMs++;
			if(countS % 3 === 0) {
				timerCounter.innerText = `Счетчик показов: ${countS / 3}`;
             slideTo(index);
			}
		};
		timers = setInterval(action,10);



			btnStop.addEventListener('click', () =>  {
				clearInterval(timers);
                btnStop.disabled = true;
				btnResume.disabled = false;
			})

			btnResume.addEventListener('click', () =>  {
				btnStop.disabled = false;
				btnResume.disabled = true;
				timers = setInterval(action,10);

			})

